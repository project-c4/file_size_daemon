SERVER = fs_daemon
CLIENT = client
CC ?= gcc
FLAGS += -g -Wall -Wextra -Wpedantic -std=c11 -D_POSIX_C_SOURCE

BIN_PATH = bin/
SRC_PATH = src/
OBJ_PATH = obj/
LIB_PATH = lib/
SRC = $(wildcard $(SRC_PATH)*.c)
LIB = $(wildcard $(LIB_PATH)*.a)

export LIBRARY_PATH = $(LIB_PATH)

all: dir $(LIB_PATH)libdaemon.a $(BIN_PATH)$(SERVER) $(BIN_PATH)$(CLIENT)

$(LIB_PATH)libdaemon.a: $(SRC_PATH)daemon.c $(SRC_PATH)daemon.h
	$(CC) $(FLAGS) -c  $(SRC_PATH)daemon.c -o $(OBJ_PATH)daemon.o
	ar crc $(LIB_PATH)libdaemon.a $(OBJ_PATH)daemon.o

$(BIN_PATH)$(SERVER): $(SRC_PATH)server.c $(LIB)
	$(CC) $(FLAGS) -c  $(SRC_PATH)server.c -o $(OBJ_PATH)server.o
	$(CC) $(FLAGS) $(OBJ_PATH)server.o -L. -lpopt -ldaemon -o $(BIN_PATH)$(SERVER)

$(BIN_PATH)$(CLIENT) : $(SRC_PATH)client.c
	$(CC) $(FLAGS) -c  $(SRC_PATH)client.c -o $(OBJ_PATH)client.o
	$(CC) $(FLAGS) $(OBJ_PATH)client.o -o $(BIN_PATH)$(CLIENT)

dir:
	if [ ! -r ./obj/ ]; then mkdir obj; fi
	if [ ! -r ./lib/ ]; then mkdir lib; fi
	if [ ! -r ./bin/ ]; then mkdir bin; fi

clean:
	if [ -r ./obj/ ]; then rm -rf $(OBJ_PATH); fi
	if [ -r ./lib/ ]; then rm -rf $(LIB_PATH); fi
	if [ -r ./bin/ ]; then rm -rf $(BIN_PATH); fi
