#include <stdio.h>
#include <stddef.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>

#define NAME "echo.socket"

int main()
{
    int fd, size;
    char buf[1024];
    struct sockaddr_un un;
    un.sun_family = AF_UNIX;

    strcpy(un.sun_path, NAME);

    if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        perror("socket");
    }

    size = offsetof(struct sockaddr_un, sun_path) + strlen(un.sun_path);
    if ((connect(fd, (struct sockaddr *)&un, size)) != 0)
    {
        perror("connect");
    }

    while (recv(fd, buf, 1024, 0) > 0)
    {
        printf(buf);
    }

    return 0;
}