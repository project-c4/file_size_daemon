#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <popt.h>
#include <syslog.h>
#include <errno.h>
#include <stdint.h>
#include <poll.h>
#include <fcntl.h>

#include "daemon.h"

#define CONF "conf/fs_daemon.conf"
#define NAME "echo.socket"

char *read_conf()
{
    FILE *conf;
    char *f_name;
    char buf[1024];

    if ((conf = fopen(CONF, "r")) == NULL)
    {
        return NULL;
    }

    while (fgets(buf, 1024, conf) != NULL)
    {
        char *p_buf = buf;

        if (p_buf[0] == '#')
        {
            continue;
        }

        if ((p_buf = strtok(buf, ":")) != NULL)
        {
            if (strcmp(p_buf, "FS") == 0)
            {
                p_buf = strtok(NULL, " ;");

                if (p_buf != NULL)
                {
                    f_name = (char *) calloc(strlen(p_buf) + 1, sizeof(char));
                    strcpy(f_name, p_buf);
                }
            }
        }
        
    }

    fclose(conf);
    return f_name;
}

int main(int argc, const char *argv[])
{
    int c;
    poptContext optCon;
    struct poptOption optionsTable[] = {
        { "daemon", 'd', 0, 0, 'd',
        "use daemonize", NULL },
        POPT_AUTOHELP
        { NULL, 0, 0, 0, 0, NULL, NULL }
    };
    int sock, msgsock;
    int conf;
    struct sockaddr_un server;
    char buf[1024];
    char *f_name = NULL;
    struct pollfd fds[2];
    struct stat info_file;

    info_file.st_size = 0;

    optCon = poptGetContext(NULL, argc, argv, optionsTable, 0);

    while ((c = poptGetNextOpt(optCon)) >= 0) 
    {
        switch (c) 
        {
            case 'd': daemonize("daemon.log"); break;
            default: break;
        }
    }

    if ((f_name = read_conf()) == NULL)
    {
        char *errorbuf = strerror(errno);
        syslog(LOG_CRIT, "%s : %s", CONF, errorbuf);
        return errno;
    }

    conf = open(f_name, O_RDONLY);
    if (conf < 0)
    {
        perror(f_name);
        return 0;
    }

    sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock < 0) 
    {
        char *errorbuf = strerror(errno);
        syslog(LOG_CRIT, errorbuf);
        exit(1);
    }

    fds[0].fd = sock;
    fds[0].events = POLLIN;

    fds[1].fd = conf;
    fds[1].events = POLLIN;

    server.sun_family = AF_UNIX;
    strcpy(server.sun_path, NAME);
    if (bind(sock, (struct sockaddr *) &server, sizeof(struct sockaddr_un))) 
    {
        char *errorbuf = strerror(errno);
        syslog(LOG_CRIT, errorbuf);
        exit(1);
    }
    listen(sock, 5);
    for (;;) {
        int ret = poll( fds, 2, 10000 );
        if (ret == -1 )
            continue;
        else if ( ret == 0 )
            continue;
        else
        {
            if (fds[0].revents & POLLIN)
            {
                fds[0].revents = 0;
                msgsock = accept(sock, 0, 0);
                if (msgsock == -1)
                {
                    char *errorbuf = strerror(errno);
                    syslog(LOG_CRIT, errorbuf);
                    break;
                }
                else
                {
                    send(msgsock, buf, strlen(buf), 0);
                }
                close(msgsock);
            }

            if (fds[1].revents & POLLIN)
            {
                fds[1].revents = 0;
                fstat (conf, &info_file);
                sprintf(buf, "%s size: %ld\n", f_name, info_file.st_size);
            }
        }
        
    }
    close(sock);
    close(conf);
    unlink(NAME);
}
