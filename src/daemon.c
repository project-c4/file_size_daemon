#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#include "daemon.h"

#define log "daemon.log"
void daemonize(char *log_name)
{
    int fd0, fd1, fd2;
    pid_t pid;
    struct rlimit rl;
    struct sigaction sa;

    if (log_name != NULL)
    {
        openlog(log_name, LOG_CONS, LOG_DAEMON);
    }

    umask(0); /* Сбросить маску режима создания файла. */

    if (getrlimit(RLIMIT_NOFILE, &rl) < 0)  /* Получить максимально возможный номер дескриптора файла. */
    {
        perror("невозможно получить максимальный номер дескриптора");
    }

    if ((pid = fork()) < 0) 
    {
        perror("ошибка вызова функции fork");
    }
    else if (pid != 0) /* родительский процесс */
    {
        exit(0);
    }

    /* Стать лидером нового сеанса, чтобы утратить управляющий терминал. */
    setsid();
    
    /* Обеспечить невозможность обретения управляющего терминала в будущем. */
    sa.sa_handler = SIG_IGN;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGHUP, &sa, NULL) < 0)
    {
        syslog(LOG_CRIT, "невозможно игнорировать сигнал SIGHUP");
    }
    if ((pid = fork()) < 0)
    {
        syslog(LOG_CRIT, "ошибка вызова функции fork");
    }
    else if (pid != 0) /* родительский процесс */
    {
        exit(0);
    }

    /* Закрыть все открытые файловые дескрипторы. */
    if (rl.rlim_max == RLIM_INFINITY) 
    {
        rl.rlim_max = 1024;
    }
    for (int i = 0; i < (int) rl.rlim_max; i++)
    {
        close(i);
    }

    /* Присоединить файловые дескрипторы 0, 1 и 2 к /dev/null. */
    fd0 = open("/dev/null", O_RDWR); 
    fd1 = dup(0);
    fd2 = dup(0);
    if (fd0 != 0 || fd1 != 1 || fd2 != 2)
    {
        syslog(LOG_CRIT, "ошибочные файловые дескрипторы %d %d %d", fd0, fd1, fd2);
    }
    return;
}